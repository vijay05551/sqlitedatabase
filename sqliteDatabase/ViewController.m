//
//  ViewController.m
//  sqliteDatabase
//
//  Created by osx on 03/08/16.
//  Copyright © 2016 Ameba Technologies. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSString *message;
    UIAlertView *alert;
    BOOL isPermission;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isPermission = false;
    
    alert = [[UIAlertView alloc]initWithTitle:@""
                                                   message:message
                                                  delegate:nil
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (IBAction)submitBtnAction:(id)sender
{
    if (self.enterRegNoTextField.text.length == 0)
    {
        isPermission = false;
        message = @"Reg number field can't be blank.";
        alert.message = message;
    }
    else if (self.enterNameTextField.text.length == 0)
    {
        isPermission = false;
        message = @"Name field can't be blank.";
        alert.message = message;
    }
    else if (self.departmentTextField.text.length == 0)
    {
        isPermission = false;
        message = @"Department field can't be blank.";
        alert.message = message;
    }
    else if (self.yearTextField.text.length == 0)
    {
        isPermission = false;
        message = @"Year field can't be blank.";
        alert.message = message;
    }
    else
    {
        isPermission = true;
    }
    
    if (isPermission)
    {
//        isPermission = [[DBManager getSharedInstance]saveDataWithReg:self.enterRegNoTextField.text withName:self.enterNameTextField.text withDepartment:self.departmentTextField.text withYear:self.yearTextField.text];
        
        isPermission = [[DBManager getSharedInstance]saveData:self.enterRegNoTextField.text
                                                         name:self.enterNameTextField.text
                                                   department:self.departmentTextField.text
                                                         year:self.yearTextField.text];
        
        if (isPermission == NO)
        {
            message = @"Data Insertion failed";
            alert.message = message;
        }
        else
        {
            self.enterRegNoTextField.text      = @"";
            self.enterNameTextField.text       = @"";
            self.departmentTextField.text      = @"";
            self.yearTextField.text            = @"";
            
            message = @"Data Saved";
            alert.message = message;
        }
    }
    [alert show];
}

- (IBAction)findRegBtnAction:(id)sender
{
    if (self.findRegNoTextField.text.length != 0)
    {
        
        NSArray *data = [[DBManager getSharedInstance]findByRegisterNumber:self.findRegNoTextField.text];
        if (data == nil)
        {
            message = @"Data not found";
            alert.message = message;
            isPermission = false;
        }
        else
        {
            message = @"Data found";
            alert.message = message;
            isPermission = true;
            
            self.enterRegNoTextField.text     = self.findRegNoTextField.text;
            self.enterRegNoTextField.text       = [data objectAtIndex:0];
            self.departmentTextField.text = [data objectAtIndex:1];
            self.yearTextField.text       = [data objectAtIndex:2];
        }
    }
    else
    {
        message = @"Search field can't be blank.";
        alert.message = message;
    }
    
    [alert show];
}

@end
