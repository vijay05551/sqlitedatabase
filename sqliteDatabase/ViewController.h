//
//  ViewController.h
//  sqliteDatabase
//
//  Created by osx on 03/08/16.
//  Copyright © 2016 Ameba Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *findRegNoTextField;

@property (strong, nonatomic) IBOutlet UITextField *enterRegNoTextField;

@property (strong, nonatomic) IBOutlet UITextField *enterNameTextField;

@property (strong, nonatomic) IBOutlet UITextField *departmentTextField;

@property (strong, nonatomic) IBOutlet UITextField *yearTextField;

- (IBAction)submitBtnAction:(id)sender;
- (IBAction)findRegBtnAction:(id)sender;



@end

